var page_tasks =
[
    [ "Encoder Task", "page_encoder_task.html", [
      [ "Introduction", "page_encoder_task.html#sec_encoder_task_intro", null ],
      [ "Finite State Machine", "page_encoder_task.html#sec_encoder_task_fsm", [
        [ "State Transition Diagram", "page_encoder_task.html#sec_encoder_task_trans", null ]
      ] ],
      [ "Implementation", "page_encoder_task.html#sec_encoder_task_imp", null ]
    ] ],
    [ "Motor Task", "page_motor_task.html", [
      [ "Introduction", "page_motor_task.html#sec_motor_task_intro", null ],
      [ "Finite State Machine", "page_motor_task.html#sec_motor_task_fsm", [
        [ "State Transition Diagram", "page_motor_task.html#sec_motor_task_trans", null ]
      ] ],
      [ "Implementation", "page_motor_task.html#sec_motor_task_imp", null ]
    ] ],
    [ "Temperature Sensor Task", "page_tsensor_task.html", [
      [ "Introduction", "page_tsensor_task.html#sec_tsensor_task_intro", null ],
      [ "Finite State Machine", "page_tsensor_task.html#sec_tsensor_task_fsm", [
        [ "State Transition Diagram", "page_tsensor_task.html#sec_tsensor_task_trans", null ]
      ] ],
      [ "Implementation", "page_tsensor_task.html#sec_tsensor_task_imp", null ]
    ] ],
    [ "controller Task", "page_controller_task.html", [
      [ "Introduction", "page_controller_task.html#sec_controller_task_intro", null ],
      [ "Finite State Machine", "page_controller_task.html#sec_controller_task_fsm", [
        [ "State Transition Diagram", "page_controller_task.html#sec_controller_task_trans", null ]
      ] ],
      [ "Implementation", "page_controller_task.html#sec_controller_task_imp", null ]
    ] ]
];