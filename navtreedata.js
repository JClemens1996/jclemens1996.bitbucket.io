/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation for Joshua Clemens, Cal Poly SLO 06/09/20", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Motor Driver", "index.html#sec_mot", null ],
    [ "Encoder Driver", "index.html#sec_enc", null ],
    [ "Proportional Controller", "index.html#sec_controller", null ],
    [ "Inertial Measurement Unit", "index.html#sec_IMU", null ],
    [ "Main Scripts", "index.html#sec_main", null ],
    [ "IMU Driver Details", "test.html", [
      [ "Testing Procedures", "test.html#sec_testing", null ],
      [ "Response Video", "test.html#sec_response", null ]
    ] ],
    [ "Term Project Description & Details", "TPD_6D.html", "TPD_6D" ],
    [ "Project Proposal", "Project.html", [
      [ "Problem Statement", "Project.html#PS", null ],
      [ "Project Requirements", "Project.html#PR", null ],
      [ "Bill of Materials", "Project.html#BoM", null ],
      [ "Safety Assessment", "Project.html#SA", null ]
    ] ],
    [ "Controller Tuning", "Controller.html", [
      [ "Tuning Plots", "Controller.html#sec_tuning", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Controller.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';