var TPD_6D =
[
    [ "Overview", "TPD_6D.html#O", null ],
    [ "Design Description", "TPD_6D.html#DD", null ],
    [ "CAD Model", "TPD_6D.html#CAD", null ],
    [ "Model Prototype", "TPD_6D.html#MP", null ],
    [ "Multitasking Structure", "TPD_6D.html#MS", null ],
    [ "Tasks", "TPD_6D.html#T", null ],
    [ "Closed-loop control", "TPD_6D.html#CLC", null ],
    [ "Sensors & Actuators", "TPD_6D.html#SandA", null ],
    [ "Demonstration", "TPD_6D.html#Video", null ],
    [ "Example Usage", "TPD_6D.html#EU", null ],
    [ "Task List", "page_tasks.html", "page_tasks" ]
];