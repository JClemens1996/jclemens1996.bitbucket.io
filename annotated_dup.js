var annotated_dup =
[
    [ "controller", null, [
      [ "P_controller", "classcontroller_1_1P__controller.html", "classcontroller_1_1P__controller" ]
    ] ],
    [ "controller_task", null, [
      [ "Controller_Task", "classcontroller__task_1_1Controller__Task.html", "classcontroller__task_1_1Controller__Task" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "encoder_task", null, [
      [ "Encoder_Task", "classencoder__task_1_1Encoder__Task.html", "classencoder__task_1_1Encoder__Task" ]
    ] ],
    [ "IMU_Controller", null, [
      [ "IMU", "classIMU__Controller_1_1IMU.html", "classIMU__Controller_1_1IMU" ]
    ] ],
    [ "motor", null, [
      [ "MotorDriver", "classmotor_1_1MotorDriver.html", "classmotor_1_1MotorDriver" ]
    ] ],
    [ "motor_task", null, [
      [ "Motor_Task", "classmotor__task_1_1Motor__Task.html", "classmotor__task_1_1Motor__Task" ]
    ] ],
    [ "tsensor_task", null, [
      [ "Tsensor_Task", "classtsensor__task_1_1Tsensor__Task.html", "classtsensor__task_1_1Tsensor__Task" ]
    ] ]
];